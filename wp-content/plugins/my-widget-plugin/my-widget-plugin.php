<?php

/**
 * Plugin Name:       My Widget Plugin
 * Description:       Handle the basics with this plugin.
 * Author:            Oleg Vartanov
 * Text Domain:       my-text-domain
 * Domain Path:       /languages
 */
 
require_once('classes/class-hello-world-widget.php');

function mwp_register_custom_widgets() {
	register_widget( 'Hello_World_Widget' );
}
add_action( 'widgets_init', 'mwp_register_custom_widgets' );
 
// Register widget
function mwp_widget_setup() {
	mwp_register_custom_widgets();
} 
add_action( 'init', 'mwp_widget_setup' );

// Activate the plugin
function mwp_my_widget_plugin_activate() {
	mwp_widget_setup();
}
register_activation_hook( __FILE__, 'mwp_my_widget_plugin_activate' );