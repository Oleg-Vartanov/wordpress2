<?php

class Hello_World_Widget extends WP_Widget {

  // Register widget with WordPress.
  public function __construct() {
    parent::__construct(
      'hello_world_widget', // Base ID
      'Hello World Widget', // Name
      array( 'description' => __( 'A Hello World Widget', 'my-text-domain' ), ) // Args
    );
  }

  // Front-end display of widget.
  public function widget( $args, $instance ) {
    echo '<h2 style="color:' . $instance['font-color'] . '">' . __( 'Hello World!', 'my-text-domain' ) . '</h2>';
		
		$args = array(
			'posts_per_page' => 5,
			'orderby' => 'modified'
		);
		
		$query = new WP_Query( $args );
		
		if ( $query->have_posts() ) {
			echo '<ul>';
			while ( $query->have_posts() ) {
				$query->the_post();
				echo '<li>';
				echo the_post_thumbnail( array( 50, 50 ) );	
				echo '<a href="' . get_post_permalink() . '">' . get_the_title() . '</a>';
				echo mb_substr( strip_tags( get_the_content() ), 0, 200 );
				echo '</li>';
			}
			echo '</ul>';
		} else {
			echo 'Where is no posts.';
		}
  }

  // Back-end widget form.
  public function form( $instance ) {
    ?>
    <select name="<?php echo $this->get_field_name( 'font-color' ); ?>" size="3" value="<?php echo esc_attr( $instance['font-color'] ); ?>">
      <option>red</option>
      <option>green</option>
      <option>blue</option>
    </select>
	<?php 
  }

  // Sanitize widget form values as they are saved.
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['font-color'] = ( !empty( $new_instance['font-color'] ) ) ? strip_tags( $new_instance['font-color'] ) : '';

    return $instance;
  }
 
}