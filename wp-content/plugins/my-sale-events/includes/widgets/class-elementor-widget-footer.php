<?php
/**
 * elementor widget
 */

class Sale_Events_Widget_Footer extends \Elementor\Widget_Base {

	// return widget name
	public function get_name() {
		return 'sale-events-footer';
	}

	// return widget title
	public function get_title() {
		return __( 'Sale Events Footer', 'my-text-domain' );
	}

  // return widget icon
	public function get_icon() {
		return 'fa fa-dollar';
	}

  // return the list of categories the widget belongs to
	public function get_categories() {
		return [ 'general' ];
	}

	// Adds different input fields to allow the user to change and customize the widget settings.
	protected function _register_controls() {

    // Content
		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'my-text-domain' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

    // output sale_events posts in select options
    $sale_events_options = array();
    $posts = get_posts( array( 'post_type' => 'sale_events' ) );
		foreach( $posts as $post ) {
      $sale_events_options[$post->ID] = __( $post->post_title, 'my-text-domain' );
    }

    $this->add_control(
			'sale_event',
			[
				'label' => __( 'Sale Event', 'my-text-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				//'default' => 'First SE',
				'options' => $sale_events_options,
			]
		);

		$this->end_controls_section();

    // Style
    $this->start_controls_section(
			'style_section',
			[
				'label' => __( 'Style', 'my-text-domain' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);

    $this->add_control(
			'width',
			[
				'label' => __( 'Width', 'my-text-domain' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => '%',
					//'size' => 50,
				],
				'selectors' => [
					'{{WRAPPER}} .wrapper' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

    $this->add_control(
			'height',
			[
				'label' => __( 'Height', 'my-text-domain' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1000,
						'step' => 5,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => '%',
					//'size' => 50,
				],
				'selectors' => [
					'{{WRAPPER}} .wrapper' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

    $this->add_control(
			'text_color',
			[
				'label' => __( 'Text Color', 'my-text-domain' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'scheme' => [
					'type' => \Elementor\Scheme_Color::get_type(),
					'value' => \Elementor\Scheme_Color::COLOR_1,
				],
				'selectors' => [
					'{{WRAPPER}} .title ' => 'color: {{VALUE}}',
				],
			]
		);

    $this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'label' => __( 'Background', 'my-text-domain' ),
				'types' => [ 'classic' ], // [ 'classic', 'gradient', 'video' ]
				'selector' => '{{WRAPPER}} .sale-events-elementor-widget',
			]
		);

		$this->end_controls_section();

	}

  // render widget output
	protected function render() {

		$settings = $this->get_settings_for_display();
		$event_post_id = $settings['sale_event'];
		$product_id = get_post_meta( $event_post_id, 'included_product', true );
		$post = get_post( $event_post_id );

		// setting orders goals
		if ( get_field( 'goal_b_min_orders_b', $event_post_id ) ) { // if there is both goals
			$final_orders_goal = get_field( 'goal_b_min_orders_b', $event_post_id );
			$inbetween_orders_goal = get_field( 'goal_a_min_orders_a', $event_post_id );
		} else { // there is only Goal A
			$final_orders_goal = get_field( 'goal_a_min_orders_a', $event_post_id );
		}

		$current_price = se_calc_product_price( $event_post_id, $final_orders_goal, $inbetween_orders_goal );
		$event_total_sales = se_get_event_sales( $event_post_id );
		$regular_price = get_post_meta( $product_id, '_regular_price', true );

		if ( se_is_active( $event_post_id ) ) {

			//bar percentage
			if ( $final_orders_goal > 0 ) {
				$progress = $event_total_sales / $final_orders_goal * 100;
				if ( $inbetween_orders_goal ) {
					$progress_inbetween = $inbetween_orders_goal / $final_orders_goal * 100;
				}
			} else {
				$progress = 0;
				_e( 'Event goals are not set', 'my-text-domain' );
			}

			?>
			<div class="sale-events-elementor-widget">
				<div class="wrapper text-center pl-4" style="text-align: center;">

					<h2 class="title" style="color: <?php echo $settings['text_color'] ?>;"><?php echo  $post->post_title ?></h2>

					<!-- progress bar upper captions -->
					<div class="row mb-1">
						<div class="col-sm text-left">
							<h4 style="color: <?php echo $settings['text_color'] ?>;">
								<?php echo get_field( 'starting_price', $event_post_id ) . get_field( 'currency', $event_post_id ); ?>
							</h4>
						</div>
						<div class="two-caption" style="right: <?php echo 97 - $progress_inbetween ?>%;">
							<h4 style="color: <?php echo $settings['text_color'] ?>;">
								<?php echo get_field( 'goal_a_price_a', $event_post_id ) . get_field( 'currency', $event_post_id ); ?>
							</h4>
						</div>
						<div class="col-sm text-right">
							<h4 style="color: <?php echo $settings['text_color'] ?>;">
								<?php echo get_field( 'goal_b_price_b', $event_post_id ) . get_field( 'currency', $event_post_id ); ?>
							</h4>
						</div>
					</div>

					<div class="progress mb-3">
						<div class="one success-color ml-2"></div>
						<?php if ( $inbetween_orders_goal ) : ?>
							<div
								class="two <?php echo ( $progress >= $progress_inbetween ? 'success-color' : 'no-color' ) ?>"
								style="left: <?php echo $progress_inbetween ?>%">
							</div>
						<?php endif; ?>
						<div class="three <?php echo ( $progress >= 100 ? 'success-color' : 'no-color' ) ?>"></div>
						<div class="progress-bar primary-color" style="width: <?php echo $progress ?>%"></div> 
					</div>

					<!-- progress bar lower captions -->
					<div class="row mb-3">
						<div class="col-sm text-left">
							<h4  style="color: <?php echo $settings['text_color'] ?>;">0 orders</h4>
						</div>
						<div class="two-caption" style="right: <?php echo 97 - $progress_inbetween ?>%;">
							<h4 style="color: <?php echo $settings['text_color'] ?>;"><?php echo get_field( 'goal_a_min_orders_a', $event_post_id ) ?> orders</h4>
						</div>
						<div class="col-sm text-right">
							<h4 style="color: <?php echo $settings['text_color'] ?>;"><?php echo get_field( 'goal_b_min_orders_b', $event_post_id ) ?> orders</h4>
						</div>
					</div>
					
					<p class="m-0 mb-3" style="color: <?php echo $settings['text_color'] ?>;">
						Current price: <strike><?php echo $regular_price ?></strike> <?php echo $current_price . get_field( 'currency', $event_post_id ) ?>
					</p>
					<p class="m-0" style="color: <?php echo $settings['text_color'] ?>;">Current orders: <?php echo $event_total_sales ?></p>

				</div>
			</div>
			<?php

		}	else {

			_e( 'Sale Event is not active', 'my-text-domain' );
			
		}

	}
	
}
