<?php

function se_is_active( $event_post_id ) {
	$start_date = strtotime( get_post_meta( $event_post_id, 'time_interval_start_date', true ) );
	$end_date = strtotime( get_post_meta( $event_post_id, 'time_interval_end_date', true ) );
	
	return $start_date <= time() && time() <= $end_date;
}

function se_get_remaining_time( $event_post_id ) {
	$end_date = strtotime( get_post_meta( $event_post_id, 'time_interval_end_date', true ) );
	$remaining_time = $end_date - time();

	if ( date( 'j', $remaining_time ) > 0) {
		$remaining_time = date( 'j', $remaining_time ) . ' Days';
	} elseif ( date( 'G', $remaining_time ) > 0 ) {
		$remaining_time = date( 'G', $remaining_time ) . ' Hours';
	} else {
		$remaining_time = intval( date( 'i', $remaining_time ) ) . ' Minutes';
	}

	return $remaining_time;
}

// get product's total_sales in a time interval 
function se_get_event_sales( $event_post_id ) {
	global $wpdb;
	$product_id = get_post_meta( $event_post_id, 'included_product', true );
	$date_from = get_post_meta( $event_post_id, 'time_interval_start_date', true );
	$date_to = get_post_meta( $event_post_id, 'time_interval_end_date', true );

	$sql = "
	SELECT COUNT(*) AS sale_count
	FROM {$wpdb->prefix}woocommerce_order_items AS order_items
	INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS order_meta ON order_items.order_item_id = order_meta.order_item_id
	INNER JOIN {$wpdb->posts} AS posts ON order_meta.meta_value = posts.ID
	WHERE order_items.order_item_type = 'line_item'
	AND order_meta.meta_key = '_product_id'
	AND order_meta.meta_value = %d
	AND order_items.order_id IN (
			SELECT posts.ID AS post_id
			FROM {$wpdb->posts} AS posts
			WHERE posts.post_type = 'shop_order'
					AND posts.post_status IN ('wc-completed','wc-processing')
					AND DATE(posts.post_date) BETWEEN %s AND %s
	)
	GROUP BY order_meta.meta_value";

	$fake_sales = get_post_meta( $event_post_id, 'fake_sales' , true );
	$event_total_sales = $wpdb->get_var($wpdb->prepare($sql, $product_id, $date_from, $date_to));
	update_post_meta( $event_post_id, 'total_sales', $event_total_sales );
	return $event_total_sales + $fake_sales;
}

function se_calc_product_price( $event_post_id, $final_orders_goal, $inbetween_orders_goal ) {
	$product_id = get_post_meta( $event_post_id, 'included_product', true );
	$event_total_sales = se_get_event_sales( $event_post_id );

	if ( se_is_active( $event_post_id ) ) {
		if ( ( $inbetween_orders_goal && $event_total_sales < $inbetween_orders_goal ) || ( ! $inbetween_orders_goal && $event_total_sales < $final_orders_goal ) ) {
			$new_price = get_field( 'starting_price', $event_post_id );
		} elseif ( 
			( $inbetween_orders_goal && $event_total_sales >= $inbetween_orders_goal && $event_total_sales < $final_orders_goal )
			|| ( ! $inbetween_orders_goal && $event_total_sales >= $final_orders_goal )
		) {
			$new_price = get_field( 'goal_a_price_a', $event_post_id );
		} else {
			$new_price = get_field( 'goal_b_price_b', $event_post_id );
		}
	} else {
		$new_price = get_post_meta( $product_id, '_regular_price', true );
	}

	se_update_product_price( $product_id, $new_price );
	return $new_price;
}

function se_update_product_price( $product_id, $new_price ) {
	
	// update variable product price
	$product = new WC_Product_Variable( $product_id );
	if( $product->is_type( 'variable' ) ) { 
		foreach( $product->get_available_variations() as $variation_values ) {
			$variation_id = $variation_values['variation_id'];
			update_post_meta( $variation_id, '_sale_price', $new_price );
			update_post_meta( $variation_id, '_price', $new_price );
			wc_delete_product_transients( $variation_id ); // Clear/refresh the variation cache
		}
	}

	update_post_meta( $product_id, '_sale_price', $new_price );
	update_post_meta( $product_id, '_price', $new_price );
	wc_delete_product_transients( $product_id ); // Clear/refresh the variable product cache
}