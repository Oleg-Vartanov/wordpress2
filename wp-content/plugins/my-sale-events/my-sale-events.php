<?php
/**
 * Plugin Name:       My Sale Events
 * Description:       Adds sales event type.
 * Author:            Oleg Vartanov
 * Text Domain:       my-text-domain
 * Domain Path:       /languages
 */

require __DIR__ . '/includes/widget-functions.php';

function mse_load_stylesheets() {
	wp_enqueue_style( 'bootstrap-css', plugin_dir_url( __FILE__ ) . 'assets/css/bootstrap/bootstrap.min.css', array(), false, 'all' );
  wp_enqueue_style( 'sale-events-css', plugin_dir_url( __FILE__ ) . 'assets/css/style.css', array(), false, 'all' ); //always put our own styles as last
};
add_action( 'wp_enqueue_scripts', 'mse_load_stylesheets' );

add_action( 'elementor/widgets/widgets_registered', function( $widgets_manager ) {
	require __DIR__ . '/includes/widgets/class-elementor-widget.php';
	require __DIR__ . '/includes/widgets/class-elementor-widget-footer.php';
    
  $widgets_manager->register_widget_type( new Sale_Events_Widget() );
	$widgets_manager->register_widget_type( new Sale_Events_Widget_Footer() );
} );


// update product meta on sale events meta save
function save_sale_events_callback( $meta_id, $object_id, $meta_key, $meta_value ){
	$product_id = get_post_meta( $object_id, 'included_product', true ); 

	switch ( $meta_key ) {
    case 'time_interval_start_date':
			$start_date = strtotime( get_post_meta( $object_id, 'time_interval_start_date', true ) );	
			update_post_meta( $product_id, '_sale_price_dates_from', $start_date );
		break;
    case 'time_interval_end_date':
			$end_date = strtotime( get_post_meta( $object_id, 'time_interval_end_date', true ) );
			update_post_meta( $product_id, '_sale_price_dates_to', $end_date );
		break;
    case 'starting_price':
			$sale_price = get_post_meta( $object_id, 'starting_price', true );
			update_post_meta( $product_id, '_sale_price', $sale_price );
		break;
		case 'increase_gradually':
		case 'orders_every_minutes_minutes':
		case 'orders_every_minutes_order_every':
			$orders_step =  get_post_meta( $object_id, 'orders_every_minutes_order_every', true );
			$cron_interval = get_post_meta( $object_id, 'orders_every_minutes_minutes', true );
			if ( get_post_meta( $object_id, 'increase_gradually', true ) ) {
				clean_cron_from( $object_id, $orders_step );
				activate_cron( $object_id, $orders_step, $cron_interval );
			} else {
				deactivate_cron( $object_id, $orders_step, $cron_interval );
			}
		break;
	}
}
add_action( 'updated_postmeta', 'save_sale_events_callback' , 10, 4 );

function register_post_type_sale_events() {  
	$labels = [
		"name" => __( "Sale Events", "myfirsttheme" ),
		"singular_name" => __( "Sale Event", "myfirsttheme" ),
    "add_new" => __( "Add new", "myfirsttheme" ),
		"add_new_item" => __( "Add new Sale Event", "myfirsttheme" ),
    "edit_item" => __( "Edit Sale Event", "myfirsttheme" ),
		"new_item" => __( "New Sale Event", "myfirsttheme" ),
		"view_item" => __( "View Sale Event", "myfirsttheme" ),
		"view_items" => __( "View Sale Events", "myfirsttheme" ),
		"search_items" => __( "Search Sale Events", "myfirsttheme" ),
		"not_found" => __( "No Sale Events found", "myfirsttheme" ),
		"not_found_in_trash" => __( "No Sale Events found in trash", "myfirsttheme" ),
    "parent_item_colon" => __( "Parent Sale Event:", "myfirsttheme" ),
    "all_items" => __( "All Sale Events", "myfirsttheme" ),
    "archives" => __( "Sale Events archives", "myfirsttheme" ),
    "attributes" => __( "Sale Events attributes", "myfirsttheme" ),
    "insert_into_item" => __( "Insert into Sale Event", "myfirsttheme" ),
    "uploaded_to_this_item" => __( "Upload to this Sale Event", "myfirsttheme" ),
    "featured_image" => __( "Featured image for this Sale Event", "myfirsttheme" ),
    "set_featured_image" => __( "Set featured image for this Sale Event", "myfirsttheme" ),
    "remove_featured_image" => __( "Remove featured image for this Sale Event", "myfirsttheme" ),
    "use_featured_image" => __( "Use as featured image for this Sale Event", "myfirsttheme" ),
    "menu_name" => __( "Sale Events", "myfirsttheme" ),
    "filter_items_list" => __( "Filter Sale Events list", "myfirsttheme" ),
		"items_list_navigation" => __( "Sale Events list navigation", "myfirsttheme" ),
		"items_list" => __( "Sale Events list", "myfirsttheme" ),
		"item_published" => __( "Sale Event published", "myfirsttheme" ),
		"item_published_privately" => __( "Sale Event published privately.", "myfirsttheme" ),
		"item_reverted_to_draft" => __( "Sale Event reverted to draft.", "myfirsttheme" ),
		"item_scheduled" => __( "Sale Event scheduled", "myfirsttheme" ),
		"item_updated" => __( "Sale Event updated.", "myfirsttheme" ),
	];

	$args = [
		"label" => __( "Sale Events", "myfirsttheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
    "hierarchical" => false,
    "exclude_from_search" => false,
		"publicly_queryable" => true,
		"show_ui" => true,
    "show_in_menu" => true,
		"show_in_nav_menus" => true,
		"show_in_rest" => true,
		"rest_base" => "",
    "capability_type" => "post",
    "map_meta_cap" => true,
    "supports" => [ "title" ],
    "has_archive" => true,
    "rewrite" => [ "slug" => "sale_events", "with_front" => true ],
		"rest_controller_class" => "WP_REST_Posts_Controller",
    "query_var" => true,
		"delete_with_user" => false,	
		"menu_icon" => "dashicons-money-alt",
	];

	register_post_type( "sale_events", $args );
}
add_action( 'init', 'register_post_type_sale_events' );

// Add the custom columns to the sale_events post type:
add_filter( 'manage_sale_events_posts_columns', 'set_custom_edit_sale_events_columns' );
function set_custom_edit_sale_events_columns($columns) {
	$columns['final_sales_goal'] = __( 'Final sales goal', 'my_text_domain' );
  $columns['total_sales'] = __( 'Total sales', 'my_text_domain' );
	$columns['fake_sales'] = __( 'Fake sales', 'my_text_domain' );

  return $columns;
}

// Add the data to the custom columns for the sale_events post type:
add_action( 'manage_sale_events_posts_custom_column' , 'custom_sale_events_column', 10, 2 );
function custom_sale_events_column( $column, $post_id ) {
	switch ( $column ) {
		case 'final_sales_goal' :
			if ( get_post_meta( $post_id , 'goal_b_min_orders_b' , true ) ) {
				echo get_post_meta( $post_id , 'goal_b_min_orders_b' , true );
			} else {
				echo get_post_meta( $post_id , 'goal_a_min_orders_a' , true );
			}
		break;

		case 'total_sales' :
			echo get_post_meta( $post_id , 'total_sales' , true ); 
		break;

		case 'fake_sales' :
			echo get_post_meta( $post_id , 'fake_sales' , true ); 
		break;
	}
}

/**
 * Setting up CRON
 *
 * Schedule incriment of event's fake_sales in setted time
 *
 */
function custom_cron_schedules( $schedules ) {
	if ( ! isset( $schedules["5min"] ) ) {
		$schedules["5min"] = array(
			'interval' => 5 * 60,
			'display' => __('Once every 5 minutes'));
	}
	if ( ! isset( $schedules["30min"] ) ) {
		$schedules["30min"] = array(
			'interval' => 30 * 60,
			'display' => __('Once every 30 minutes'));
	}
	return $schedules;
}
add_filter('cron_schedules','custom_cron_schedules');

// incriment event's fake_sales in setted time
function incriment_orders( $event_post_id, $orders_step, $cron_interval ) {
	$fake_sales = get_post_meta( $event_post_id, 'fake_sales' , true );
	$fake_sales += $orders_step;
	update_post_meta( $event_post_id, 'fake_sales', $fake_sales );
}
add_action( 'custom_cron_hook', 'incriment_orders', 1, 3 );

function activate_cron( $event_post_id, $orders_step, $cron_interval ) {
	$args = array(
		'event_post_id' => $event_post_id,
		'orders_step' => $orders_step,
		'cron_interval' => $cron_interval
	);
	if ( ! wp_next_scheduled( 'custom_cron_hook', $args ) ) {
		wp_schedule_event( time(), $cron_interval, 'custom_cron_hook', $args );
	}
}

function deactivate_cron( $event_post_id, $orders_step, $cron_interval ) {
	$args = array(
		'event_post_id' => $event_post_id,
		'orders_step' => $orders_step,
		'cron_interval' => $cron_interval
	);
	wp_clear_scheduled_hook( 'custom_cron_hook', $args );
}

// deleting old cron events of the product
function clean_cron_from( $object_id, $orders_step ) {
	$intervals = array( 'hourly', 'twicedaily', 'daily', 'weekly' );
	foreach ( $intervals as $cron_interval ) {
  	deactivate_cron( $object_id, $orders_step, $cron_interval );
	}
}

// on plugin deactivation
function plugin_deactivation() {
	wp_unschedule_hook( 'custom_cron_hook' );
}
register_deactivation_hook( __FILE__, 'plugin_deactivation' ); 

/**
 * es_acf_validate_save_post
 *
 * ACF fields validation
 *
 */
function es_acf_validate_save_post() {

	$starting_price = $_POST['acf']['field_605dc5c60768a'];
	$fake_sales = $_POST['acf']['field_606ed3585a831'];
	$goal_a_min_orders_a = $_POST['acf']['field_605dd0acc29a9']['field_605dd0ffc29ab'];
	$goal_a_price_a = $_POST['acf']['field_605dd0acc29a9']['field_605dd0cac29aa'];
	$goal_b_min_orders_b = $_POST['acf']['field_605dd15557721']['field_605dd15557723'];
	$goal_b_price_b = $_POST['acf']['field_605dd15557721']['field_605dd15557722'];
	$increase_gradually = $_POST['acf']['field_605dc6c51bbd9'];
	$orders_every = $_POST['acf']['field_605dc8bccd63a']['field_605dc7651bbda'];

	// date interval check ( Start date < End date )
	if ( $_POST['acf']['field_605dcdb760e6f']['field_605dc41c6d3e5'] >= $_POST['acf']['field_605dcdb760e6f']['field_605dc4afbaa9b'] ) {
		acf_add_validation_error( 'acf[field_605dcdb760e6f]', __( 'End date must be after Start date', 'my-text-domain' ) );
	}

	$positive_condition = (
		$starting_price <= 0
		|| $fake_sales < 0
		|| $goal_a_min_orders_a <= 0
		|| $goal_a_price_a <= 0
		|| ( !empty( $goal_b_min_orders_b ) && $goal_b_min_orders_b <= 0 )
		|| ( !empty( $goal_b_price_b ) && $goal_b_price_b <= 0 )
		|| ( $increase_gradually && $orders_every <= 0 )
	);

	// fields > 0 check
	if ( $positive_condition ) { acf_add_validation_error( '', __( 'All fields must be > 0', 'my-text-domain' ) ); }

	// Goal B check
	if(
		empty( $goal_b_min_orders_b ) && !empty( $goal_b_price_b ) || !empty( $goal_b_min_orders_b ) && empty( $goal_b_price_b )
	) {
		acf_add_validation_error( 'acf[field_605dd15557721]', __( 'Goal B must be completely empty or filled', 'my-text-domain' ) );
	}

	// prices check
	if ( $starting_price <= $goal_a_price_a || ( !empty( $goal_b_price_b ) && $goal_a_price_a <= $goal_b_price_b ) ) {
		acf_add_validation_error( '', __( 'Incorrect values. Must be: "Starting price > Goal A Price > Goal B Price"', 'my-text-domain' ) );
	}

	// orders check
	if ( ( !empty( $goal_b_min_orders_b ) && $goal_a_min_orders_a >= $goal_b_min_orders_b ) ) {
		acf_add_validation_error( '', __( 'Incorrect values. Must be: "Starting orders < Goal A Min orders < Goal B Min orders"', 'my-text-domain' ) );
	}
	
}
add_action('acf/validate_save_post', 'es_acf_validate_save_post');