<?php

defined( 'ABSPATH' ) || exit; // Exit if accessed directly

class StatsWidgetBlock {
	private array $attributes = [];

    public function __construct() {
		$this->setAttributes();
		add_action( 'init', [ $this, 'onInit' ] );
	}
    
    private function setAttributes() {
	    $defaultAfter  = wp_date( 'Y-m-d', strtotime( "-1 month" ) );
	    $defaultBefore = wp_date( 'Y-m-d' );
        
        $this->attributes = [
	        'orders_date_after' => [
		        'type'    => 'string',
		        'default' => $defaultAfter,
	        ],
	        'orders_date_before' => [
		        'type'    => 'string',
		        'default' => $defaultBefore,
	        ],
	        'bs_date_after' => [
		        'type'    => 'string',
		        'default' => $defaultAfter,
	        ],
	        'bs_date_before' => [
		        'type'    => 'string',
		        'default' => $defaultBefore,
	        ],
	        'aov_date_after' => [
		        'type'    => 'string',
		        'default' => $defaultAfter,
	        ],
	        'aov_date_before' => [
		        'type'    => 'string',
		        'default' => $defaultBefore,
	        ]
        ];
    }

	public function onInit() {
		wp_register_script('swb-script', plugin_dir_url( __FILE__ ) . 'build/index.js', [ 'wp-blocks', 'wp-element' ] );
		wp_register_style( 'swb-style', plugin_dir_url( __FILE__ ) . 'build/index.css' );

		register_block_type( 'stats-widget/block-editor', [
            'attributes' => $this->attributes,
			'render_callback' => [ $this, 'renderCallback' ],
			'editor_script'   => 'swb-script',
			'editor_style'    => 'swb-style'
		] );
	}

	public function renderCallback( $attributes ) {
	    ob_start();
	    if ( class_exists( 'woocommerce' ) && !empty( $attributes ) ) {
		    sw_render_stats( $attributes );
	    }
        return ob_get_clean();
//	    return '<div class="swb-frontend">Set up dates</div>';
    }
}

new StatsWidgetBlock();