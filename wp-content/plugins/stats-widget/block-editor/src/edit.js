import {useState} from "react";

const errors = {
  startAfterEnd: '"Start date" should be earlier then "End date"',
  endAfterStart: '"End date" should be later then "Start date"',
}

export default function Edit(props) {
  const [errorMessage, setErrorMessage] = useState('');

  function handleChange(e) {
    if (!validation(e)) {
      props.setAttributes({ [e.target.name]: e.target.value })
    }
  }

  function validation(e) {
    let error = false;

    switch (e.target.name) {
      case 'orders_date_after':
        if (e.target.value >= props.attributes.orders_date_before) {
          setErrorMessage(errors.startAfterEnd);
          error = true;
        }
        break;
      case 'orders_date_before':
        if (e.target.value <= props.attributes.orders_date_after) {
          setErrorMessage(errors.endAfterStart);
          error = true;
        }
        break;
      case 'aov_date_after':
        if (e.target.value >= props.attributes.aov_date_before) {
          setErrorMessage(errors.startAfterEnd);
          error = true;
        }
        break;
      case 'aov_date_before':
        if (e.target.value <= props.attributes.aov_date_after) {
          setErrorMessage(errors.endAfterStart);
          error = true;
        }
        break;
      case 'bs_date_after':
        if (e.target.value >= props.attributes.bs_date_before) {
          setErrorMessage(errors.startAfterEnd);
          error = true;
        }
        break;
      case 'bs_date_before':
        if (e.target.value <= props.attributes.bs_date_after) {
          setErrorMessage(errors.endAfterStart);
          error = true;
        }
        break;
    }

    return error;
  }

  return (
    <div className="swb-block">
      {errorMessage && (<p className="error"> {errorMessage} </p>)}
      <div className="column">
        <h3>Orders</h3>
        <div className="field">
          <label htmlFor="orders_date_after">Start date:</label>
          <input type="date" name="orders_date_after" value={props.attributes.orders_date_after} onChange={handleChange} />
        </div>
        <div className="field">
          <label htmlFor="orders_date_before">End date:</label>
          <input type="date" name="orders_date_before" value={props.attributes.orders_date_before} onChange={handleChange} />
        </div>
      </div>
      <div className="column">
        <h3>Average order value</h3>
        <div className="field">
          <label htmlFor="orders_date_after">Start date:</label>
          <input type="date" name="aov_date_after" value={props.attributes.aov_date_after} onChange={handleChange} />
        </div>
        <div className="field">
          <label htmlFor="bs_date_before">End date:</label>
          <input type="date" name="aov_date_before" value={props.attributes.aov_date_before} onChange={handleChange} />
        </div>
      </div>
      <div className="column">
        <h3>Best Sellers</h3>
        <div className="field">
          <label htmlFor="bs_date_after">Start date:</label>
          <input type="date" name="bs_date_after" value={props.attributes.bs_date_after} onChange={handleChange} />
        </div>
        <div className="field">
          <label htmlFor="bs_date_before">End date:</label>
          <input type="date" name="bs_date_before" value={props.attributes.bs_date_before} onChange={handleChange} />
        </div>
      </div>
    </div>
  )
}