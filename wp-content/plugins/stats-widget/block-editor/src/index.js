import "./index.scss"
import Edit from './edit';

wp.blocks.registerBlockType("stats-widget/block-editor", {
  title: "Stats Widget Block",
  icon: "calculator",
  category: "common",
  edit: Edit,
  save: function () {
    return null
  }
})
