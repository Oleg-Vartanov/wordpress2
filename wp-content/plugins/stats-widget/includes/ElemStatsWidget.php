<?php

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Elementor oEmbed Widget.
 *
 * Elementor widget that inserts an embbedable content into the page, from any given URL.
 *
 * @since 1.0.0
 */
class ElemStatsWidget extends \Elementor\Widget_Base {

	/**
	 * Get widget name.
	 *
	 * Retrieve oEmbed widget name.
	 *
	 * @return string Widget name.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_name() {
		return 'statswidget';
	}

	/**
	 * Get widget title.
	 *
	 * Retrieve oEmbed widget title.
	 *
	 * @return string Widget title.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_title() {
		return esc_html__( 'Stats Widget', 'my-text-domain' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve oEmbed widget icon.
	 *
	 * @return string Widget icon.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_icon() {
		return 'eicon-code';
	}

	/**
	 * Get custom help URL.
	 *
	 * Retrieve a URL where the user can get more information about the widget.
	 *
	 * @return string Widget help URL.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_custom_help_url() {
		return 'https://developers.elementor.com/docs/widgets/';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the oEmbed widget belongs to.
	 *
	 * @return array Widget categories.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the oEmbed widget belongs to.
	 *
	 * @return array Widget keywords.
	 * @since 1.0.0
	 * @access public
	 */
	public function get_keywords() {
		return [ 'oembed', 'url', 'link' ];
	}

	/**
	 * Register oEmbed widget controls.
	 *
	 * Add input fields to allow the user to customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function register_controls() {

		// Orders section start.
		$this->start_controls_section(
			'orders_section',
			[
				'label' => esc_html__( 'Orders', 'my-text-domain' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'orders_date_after',
			[
				'label'   => esc_html__( 'After', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d', strtotime( "-1 month" ) ),
			]
		);

		$this->add_control(
			'orders_date_before',
			[
				'label'   => esc_html__( 'Before', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d' ),
			]
		);

		$this->end_controls_section();
		// Orders section end.

		// Average order value section start.
		$this->start_controls_section(
			'aov_section',
			[
				'label' => esc_html__( 'Average Order Value', 'my-text-domain' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'aov_date_after',
			[
				'label'   => esc_html__( 'After', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d', strtotime( "-1 month" ) ),
			]
		);

		$this->add_control(
			'aov_date_before',
			[
				'label'   => esc_html__( 'Before', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d' ),
			]
		);

		$user_options = [];
		foreach (get_users() as $user) {
			$user_options[$user->ID] =  __( $user->nickname, 'my-text-domain' );
		}

		$this->add_control(
			'aov_user_id',
			[
				'label'   => esc_html__( 'User', 'my-text-domain' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'options' => $user_options,
			]
		);

		$this->end_controls_section();
		// Average order value section end.

		// Best sellers section start.
		$this->start_controls_section(
			'bs_section',
			[
				'label' => esc_html__( 'Best Sellers', 'my-text-domain' ),
				'tab'   => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'bs_date_after',
			[
				'label'   => esc_html__( 'After', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d', strtotime( "-1 month" ) ),
			]
		);

		$this->add_control(
			'bs_date_before',
			[
				'label'   => esc_html__( 'Before', 'my-text-domain' ),
				'type'    => \Elementor\Controls_Manager::DATE_TIME,
				'default' => wp_date( 'Y-m-d' ),
			]
		);

		$this->end_controls_section();
		// Average order value section end.
	}

	/**
	 * Render oEmbed widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {
		$data = $this->get_settings_for_display();

		if ( class_exists( 'woocommerce' ) && !empty( $data )) {
			sw_render_stats( $data );
		}
	}
}