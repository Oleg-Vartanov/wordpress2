<?php

class StatsWidget extends WP_Widget {
    
    private string $defaultAfter  = '';
    private string $defaultBefore = '';

	// Register widget with WordPress.
	public function __construct() {
		parent::__construct(
			'stats_widget', // Base ID
			'Stats Widget', // Name
			[ 'description' => __( 'A Stats Widget', 'my-text-domain' ), ] // Args
		);

		$this->defaultAfter  = wp_date( 'Y-m-d', strtotime( "-1 month" ) );
		$this->defaultBefore = wp_date( 'Y-m-d' );
	}

	// Front-end display of widget.
	public function widget( $args, $instance ) {
		if ( class_exists( 'woocommerce' ) && !empty( $instance ) ) {
            sw_render_stats( $instance );
        }
	}

	// Back-end widget form.
	public function form( $instance ) {
		?>
        <h4>Orders</h4>
        <div>
			<?php echo __( 'Start date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'orders_date_after' ); ?>"
                   value="<?php echo $instance['orders_date_after'] ?? $this->defaultAfter; ?>">
        </div>
        <div>
			<?php echo __( 'End date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'orders_date_before' ); ?>"
                   value="<?php echo $instance['orders_date_before'] ?? $this->defaultBefore; ?>">
        </div>
        <hr/>
        <h4>Average order value</h4>
        <div>
			<?php echo __( 'Start date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'aov_date_after' ); ?>"
                   value="<?php echo $instance['aov_date_after'] ?? $this->defaultAfter; ?>">
        </div>
        <div>
			<?php echo __( 'End date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'aov_date_before' ); ?>"
                   value="<?php echo $instance['aov_date_before'] ?? $this->defaultBefore; ?>">
        </div>
        <h4>Best Sellers</h4>
        <div>
			<?php echo __( 'Start date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'bs_date_after' ); ?>"
                   value="<?php echo $instance['aov_date_after'] ?? $this->defaultAfter; ?>">
        </div>
        <div>
			<?php echo __( 'End date', 'my-text-domain' ) ?>:
            <input type="date" name="<?php echo $this->get_field_name( 'bs_date_before' ); ?>"
                   value="<?php echo $instance['aov_date_before'] ?? $this->defaultBefore; ?>">
        </div>
		<?php
	}
}