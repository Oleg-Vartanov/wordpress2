<?php

/**
 * Plugin Name:       Stats Widget
 * Description:       Adds custom widget showing stats.
 * Author:            Oleg Vartanov
 * Text Domain:       my-text-domain
 * Domain Path:       /languages
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

function sw_register_custom_widget() {
	require_once 'includes/StatsWidget.php';
	register_widget( 'StatsWidget' );
}
add_action( 'widgets_init', 'sw_register_custom_widget' );

function sw_register_list_widget( $widgets_manager ) {
	require_once 'includes/ElemStatsWidget.php';
	$widgets_manager->register( new \ElemStatsWidget() );

}
add_action( 'elementor/widgets/register', 'sw_register_list_widget' );

/**
 * Returns the number of orders placed from within a given from and to date
 * 
 * @param string $date_after
 * @param string $date_before
 * 
 * @return int Number of orders placed from within a given from and to date
 */
function sw_get_orders_number( string $date_after, string $date_before ) {
	$orders      = wc_get_orders( [
		'date_after'  => $date_after,
		'date_before' => $date_before
	] );

	return count( $orders );
}

/**
 * Returns the average order value for a given from and to date, optionally filtered by a user
 * 
 * @param string $date_after
 * @param string $date_before
 * @param string|int $user_id
 *
 * @return int Number of orders placed from within a given from and to date
 */
function sw_get_average_order_value( string $date_after, string $date_before, $user_id ) {
	$average_order_value = 0;
	$orders_count = sw_get_orders_number($date_after, $date_before);

	$params = [
		'date_after'  => $date_after, //$instance['aov-date-after'],
		'date_before' => $date_before, //$instance['aov-date-before']
	];

	if (!empty($user_id)) {
		$params['author'] = $user_id;
	}

	if ($orders_count > 0) {
		$orders = wc_get_orders( $params );

		$orders_value = 0;
		foreach ( $orders as $order ) {
			$orders_value += $order->get_total();
		}
		$average_order_value = $orders_value / $orders_count;
	}

	return $average_order_value;
}

/**
 * Returns the most purchased entity for a given period.
 *
 * @param array $data ['orders_date_after', 'orders_date_before', 'aov_date_after', 'aov_date_before', 'bs_date_after', 'bs_date_before']
 *
 */
function sw_render_stats( array $data ) {
	$ordersNumber = sw_get_orders_number( $data['orders_date_after'], $data['orders_date_before'] );

	echo '<p>' . __( 'Orders', 'my-text-domain' ) . ': ' . $ordersNumber . '</a>';

	if ( $ordersNumber > 0 ) {
		$bestSellingProducts = sw_get_best_selling_products( $data['bs_date_after'], $data['bs_date_before'] );
		$bestSellingProduct  = wc_get_product( $bestSellingProducts[0]->id );

		echo '<p>' . __( 'Average order value', 'my-text-domain' ) . ': ' . sw_get_average_order_value( $data['aov_date_after'], $data['aov_date_before'], $data['aov_user_id'] ?? '' ) . '</a>';
		echo '<p>' . __( 'Most purchased', 'my-text-domain' ) . ': <a href="' . $bestSellingProduct->get_permalink() . '">' . $bestSellingProduct->get_title() . '</a></p>';
	}
}

/**
 * Returns array of best-selling products tuple [id, sales].
 *
 * @param string $date_after
 * @param string $date_before
 * @param string|int $limit Number of returned products
 *
 * @return array
 */
function sw_get_best_selling_products( string $date_after, string $date_before, $limit = '1' ) {
	global $wpdb;

	$limit_clause = intval($limit) <= 0 ? '' : 'LIMIT '. intval($limit);

	$query = "SELECT p.ID as id, SUM(oim2.meta_value) as sold
        FROM {$wpdb->prefix}posts p
        INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta oim
            ON p.ID = oim.meta_value
        INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta oim2
            ON oim.order_item_id = oim2.order_item_id
        INNER JOIN {$wpdb->prefix}woocommerce_order_items oi
            ON oim.order_item_id = oi.order_item_id
        INNER JOIN {$wpdb->prefix}posts as o
            ON o.ID = oi.order_id
        WHERE p.post_type = 'product'
        AND p.post_status = 'publish'
        AND o.post_status IN ('wc-processing','wc-completed')
        AND o.post_date >= '$date_after'
        AND o.post_date <= '$date_before'
        AND oim.meta_key = '_product_id'
        AND oim2.meta_key = '_qty'
        GROUP BY p.ID
        ORDER BY SUM(oim2.meta_value) + 0 DESC
		$limit_clause;
	";
	
	return (array) $wpdb->get_results($query);
}

require_once 'block-editor/index.php';