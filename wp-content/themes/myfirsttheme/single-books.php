<?php get_header(); ?>

<div class="container">

  <?php if ( have_posts() ) : while( have_posts() ) : the_post(); ?>
    
    <!-- Title -->
    <h1 style="text-align:center"><?php the_title(); ?></h1>
    <!-- Owner -->
    <h3><?php _e( 'Owner', 'my-text-domain' ) ?>:</h3>
    <p><?php the_author(); ?></p>

    <?php
      $post_id = get_the_ID();
      if ( ! empty( $post_id ) ) {

        $book_cover = get_post_meta( $post_id, '_book-cover', true );
        $book_desc  = get_post_meta( $post_id, '_book-desc', true );
        $book_price = get_post_meta( $post_id, '_book-price', true );
        $book_genre = get_post_meta( $post_id, '_book-genre', true );

        // Book Cover
        if ( ! empty ( $book_cover ) ) {
          echo '<h3>' . __( 'Book Cover', 'my-text-domain' ) . ':' . '</h3>';
          echo '<img src="' . esc_url( $book_cover ) . '"style="width: 200px; height: 250px;">';
        }
        // Description
        if ( ! empty( $book_desc ) ) {
          echo '<h3>' . __( 'Description', 'my-text-domain' ) . ':' . '</h3>';
          echo '<p>' . esc_textarea( $book_desc ) . '</p>';
        }
        // Price
        if ( ! empty( $book_price ) ) {
          echo '<h3>' . __( 'Price', 'my-text-domain' ) . ':' . '</h3>';
          echo '<p>' . esc_html( $book_price ) . '$' . '</p>';
        }
        // Genre
        if ( ! empty( $book_genre ) ) {
          echo '<h3>' . __( 'Genre', 'my-text-domain' ) . ':' . '</h3>';
          echo '<p>' . implode(", ",$book_genre)  . '</p>';
        }        
      }
    ?>

  <?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>