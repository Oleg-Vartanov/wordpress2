/**
 * wp-media
 */

jQuery(function ($) {
  $('body').on('click', '.book_cover_upload_button', function (e) {
    e.preventDefault();

    var button = $(this), book_cover_uploader = wp.media({
      title: 'Custom image',
      library: { type: 'image' },
      button: { text: 'Use this image' },
      multiple: false
    }).on('select', function () {
      var attachment = book_cover_uploader.state().get('selection').first().toJSON();
      $('#book-cover').val(attachment.url);
    }).open();
  });
});