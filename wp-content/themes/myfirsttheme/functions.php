<?php

/**
 * Functions and definitions
 */

function load_stylesheets() {
  wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array(), false, 'all' );
  wp_enqueue_style( 'oleg-css', get_template_directory_uri() . '/style.css', array(), false, 'all' ); //always put our own styles as last
};
add_action( 'wp_enqueue_scripts', 'load_stylesheets' );

function include_jquery() {
  wp_deregister_script( 'jquery' ); //incase it's already registered
  wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery-3.6.0.min.js', '', 1, true ); //true -> put in the footer (not header)
  add_action( 'wp_enqueue_scripts', 'jquery' );
};
add_action( 'wp_enqueue_scripts', 'include_jquery' );

function load_js() {
  wp_enqueue_script( 'oleg-js', get_template_directory_uri() . '/js/scripts.js', '', 1, true ); //true -> put in the footer (not header)
};
add_action( 'wp_enqueue_scripts', 'load_js' );

add_theme_support( 'menus' );
add_theme_support( 'post-thumbnails' );
add_image_size( 'smallest', 300, 300, true ); //crop it 300p
add_image_size( 'largest', 800, 800, true );  //crop it 800p

register_nav_menus(
  array(
    'primary-menu'   => __( 'Primary Menu', 'my-text-domain' ),
    'secondary-menu' => __( 'Secondary Menu', 'my-text-domain' )
  )
);

function register_post_type_books() {  
	$labels = [
		"name" => __( "Books", "myfirsttheme" ),
		"singular_name" => __( "Book", "myfirsttheme" ),
    "add_new" => __( "Add new", "myfirsttheme" ),
		"add_new_item" => __( "Add new Book", "myfirsttheme" ),
    "edit_item" => __( "Edit Book", "myfirsttheme" ),
		"new_item" => __( "New Book", "myfirsttheme" ),
		"view_item" => __( "View Book", "myfirsttheme" ),
		"view_items" => __( "View Books", "myfirsttheme" ),
		"search_items" => __( "Search Books", "myfirsttheme" ),
		"not_found" => __( "No Books found", "myfirsttheme" ),
		"not_found_in_trash" => __( "No Books found in trash", "myfirsttheme" ),
    "parent_item_colon" => __( "Parent Book:", "myfirsttheme" ),
    "all_items" => __( "All Books", "myfirsttheme" ),
    "archives" => __( "Books archives", "myfirsttheme" ),
    "attributes" => __( "Books attributes", "myfirsttheme" ),
    "insert_into_item" => __( "Insert into Book", "myfirsttheme" ),
    "uploaded_to_this_item" => __( "Upload to this Book", "myfirsttheme" ),
    "featured_image" => __( "Featured image for this Book", "myfirsttheme" ),
    "set_featured_image" => __( "Set featured image for this Book", "myfirsttheme" ),
    "remove_featured_image" => __( "Remove featured image for this Book", "myfirsttheme" ),
    "use_featured_image" => __( "Use as featured image for this Book", "myfirsttheme" ),
    "menu_name" => __( "Books", "myfirsttheme" ),
    "filter_items_list" => __( "Filter Books list", "myfirsttheme" ),
		"items_list_navigation" => __( "Books list navigation", "myfirsttheme" ),
		"items_list" => __( "Books list", "myfirsttheme" ),
		"item_published" => __( "Book published", "myfirsttheme" ),
		"item_published_privately" => __( "Book published privately.", "myfirsttheme" ),
		"item_reverted_to_draft" => __( "Book reverted to draft.", "myfirsttheme" ),
		"item_scheduled" => __( "Book scheduled", "myfirsttheme" ),
		"item_updated" => __( "Book updated.", "myfirsttheme" ),
	];

	$args = [
		"label" => __( "Books", "myfirsttheme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
    "hierarchical" => false,
    "exclude_from_search" => false,
		"publicly_queryable" => true,
		"show_ui" => true,
    "show_in_menu" => true,
		"show_in_nav_menus" => true,
		"show_in_rest" => true,
		"rest_base" => "",
    "capability_type" => "post",
    "map_meta_cap" => true,
    "supports" => [ "title", "author" ],
    "has_archive" => true,
    "rewrite" => [ "slug" => "books", "with_front" => true ],
		"rest_controller_class" => "WP_REST_Posts_Controller",
    "query_var" => true,
		"delete_with_user" => false,	
		"menu_icon" => "dashicons-book-alt",
	];

	register_post_type( "books", $args );
}
add_action( 'init', 'register_post_type_books' );

function add_books_meta_box() {
  add_meta_box(
    'books-box-id',                             // Unique ID
    __( 'Books Meta Box', 'my-text-domain' ),   // Box title
    'books_meta_box_callback',                  // callback
    'books'                                     // Post type
  ); 
}
add_action( 'add_meta_boxes', 'add_books_meta_box' );

function include_media_script() {
  if ( ! did_action( 'wp_enqueue_media' ) ) {
      wp_enqueue_media();
  }
  wp_enqueue_script( 'book-cover_uploader', get_stylesheet_directory_uri() . '/js/books_img_uploader.js', array('jquery'), null, false );
}
add_action( 'admin_enqueue_scripts', 'include_media_script' );

function books_meta_box_callback( $post ) {

  echo '<table>';

    // Book cover field
    echo '<tr>';
      echo '<td style="vertical-align: top; background: #f6f6f6; padding: 5px 10px;">';
        echo '<a href="#" class="button button-secondary book_cover_upload_button">'. __( 'Upload Book Cover', 'my-text-domain' ) .'</a>';        
      echo '</td>';
      echo '<td>';
        $book_cover = get_post_meta( $post->ID, '_book-cover', true );                            
        echo '<input type="text" id="book-cover" name="book-cover" style="width:100%;" readonly="readonly" value="' . esc_url($book_cover) . '">';     
        if( strlen($book_cover) > 0 ) {
          echo '<img src="'. esc_url($book_cover).'" style="width: 200px; height: 250px; margin: 10px;">';
        }        
      echo '</td>';
    echo '</tr>';

    // Description field
    echo '<tr>';
      echo '<td style="vertical-align: top; background: #f6f6f6; padding: 5px 10px;">';
        echo '<label for="book-genre">' . __( 'Book Description', 'my-text-domain' ) . '</label> ';
      echo '</td>';
      echo '<td>';
        $book_desc = get_post_meta( $post->ID, '_book-desc', true );        
        echo '<textarea name="book-desc" id="book-desc" cols="50" rows="3" style="width:100%;" maxlength="200"
          placeholder="' . __( "This book is about ..." , "my-text-domain" ) . '">' . esc_html($book_desc) . '</textarea>';
      echo '</td>';
    echo '</tr>';

    // Price field
    echo '<tr>';
      echo '<td style="vertical-align: top; background: #f6f6f6; padding: 5px 10px;">';
        echo '<label for="book-price">' . __( 'Price', 'my-text-domain' ) . '</label> ';
      echo '</td>';
      echo '<td>';
        $book_price = get_post_meta( $post->ID, '_book-price', true );              
        echo '<input type="number" min="0.00" step="0.01" name="book-price" id="book-price" value="' . esc_html($book_price) . '">' . ' $';
      echo '</td>';
    echo '</tr>';    

    // Genre field
    echo '<tr>';
      echo '<td style="vertical-align: top; background: #f6f6f6; padding: 5px 10px;">';
        echo '<label for="book-genre">' . __( 'Genre', 'my-text-domain' ) . '</label> ';
      echo '</td>';
      echo '<td>';
        $book_genre = get_post_meta( $post->ID, '_book-genre', true );          
        echo '<select name="book-genre[]" multiple size="5" id="book-genre" value="' . esc_html($book_genre) . '">';
          echo '<option>Novel</option>';
          echo '<option>Comedy</option>';
          echo '<option>Classic</option>';
          echo '<option>Historical</option>';
          echo '<option>Fantasy</option>';
        echo '</select>';
        echo '<p>' . __( 'Select multiple using "Ctrl"', 'my-text-domain' ) . '</p>';      
      echo '</td>';
    echo '</tr>';

  echo '</table>';
}

function save_books_meta_box( $post_id ) {

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) { // if it's autosave - do nothing
      return $post_id;
  }

  $book_cover = esc_url_raw( $_POST['book-cover'] );
  $book_desc  = sanitize_text_field( $_POST['book-desc'] );
  $book_price = sanitize_text_field( $_POST['book-price'] );
  $book_genre = $_POST['book-genre'];

  update_post_meta( $post_id, '_book-cover', $book_cover );
  update_post_meta( $post_id, '_book-desc', $book_desc );
  update_post_meta( $post_id, '_book-price', $book_price );
  update_post_meta( $post_id, '_book-genre', $book_genre );
}
add_action( 'save_post', 'save_books_meta_box' );

function myfirsttheme_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Sidebar 2', 'my-text-domain' ),
    'id'            => 'sidebar-2',
    'description'   => __( 'Top-right corner', 'my-text-domain' ),
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
}
add_action( 'widgets_init', 'myfirsttheme_widgets_init' );