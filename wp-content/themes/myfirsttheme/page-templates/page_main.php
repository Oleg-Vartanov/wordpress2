<?php /* Template Name: Main Template */ ?>

<?php get_header(); ?>

<div class="container" style="text-align:center">

  <h1><?php __('Main Template', 'my-text-domain'); ?></h1>

  <h1><?php the_title(); ?></h1>

  <?php if ( has_post_thumbnail() ) : ?>
    <img src="<?php the_post_thumbnail_url( 'largest' ) ?>"> 
  <?php endif; ?>

  <?php
    if ( have_posts() ):
      while( have_posts() ):
        the_post();
        the_content();
      endwhile;
    endif;
  ?>

</div>

<?php get_footer(); ?>